<?php 

require("header.php");


?>

<html>

        <head>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">

                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>



		</head>
		<p><center><a href="add_form.php">Add Record</a> </center></p>
		<body>
			
			<table class="table table-striped table-hover text-center">
				<thead>
					<th>Id</th>
					<th>Username</th>
					<th>Password</th>
					<th>Name</th>
					<th>Age</th>
					<th>City</th>
					<th>Added_date</th>
					<th>Updated_date</th>
					<th>Status</th>
					<th>Action</th>
				</thead>
				<tbody>

					<?php foreach($records as $row) { ?>

						<tr>

							<td><?php echo $row['id']; ?></td>
							<td><?php echo $row['username']; ?></td>
							<td><?php echo $row['password']; ?></td>
							<td><?php echo $row['name']; ?></td>
							<td><?php echo $row['age']; ?></td>
							<td><?php echo $row['city']; ?></td>
							<td><?php echo $row['added_date']; ?></td>
							<td><?php echo $row['updated_date']; ?></td>
							<td><?php echo $row['status']; ?></td>
							<td>
															<button onclick="showUser(<?php echo $row['id']; ?>)" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
	  <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
	  <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
	</svg></button> &nbsp; &nbsp;
	<a href="edit_form.php?id=<?php echo $row['id']; ?>"><button class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
	  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
	  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
	</svg></button></a>
								&nbsp; &nbsp;
	<a onclick="return confirm('Are you sure?')" href="delete.php?id=<?php echo $row['id']; ?>" class = "btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
	  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
	  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
	</svg></a>
							</td>

						</tr>

					<?php } ?>

				</tbody>
			</table>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">User Details</h5>
		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
			Username: <span id="username"></span><br>
			Password: <span id="password"></span><br>
			Name: <span id="name"></span><br>
			Age: <span id="age"></span><br>
			City: <span id="city"></span><br>
	      </div>
	      <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>


		</body>
			<script>
						function showUser(id) {
						url = `getuser.php?id=${id}`;
						fetch(url)
								.then(data=>data.json())
								.then(data=> {

                                                                document.getElementById("username").innerHTML = data['username'];
                                                                document.getElementById("password").innerHTML = data['password'];
                                                                document.getElementById("name").innerHTML = data['name'];
                                                                document.getElementById("age").innerHTML = data['age'];
                                                                document.getElementById("city").innerHTML = data['city'];
                                                        }
                                                        );
                        }
                </script>
</html>
